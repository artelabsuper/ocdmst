# OCdmst
This is a project about one-class classifiers based on minimum spanning tree. This version is an extendend methodology based on dynamic boundary decision of our previous works where we achiveve the state-of-the-art in one-class task on different UCI datasets repository. If you use the code or extend it please cite our work (bibtex at below). If you have interest to read other paper based on the same field visit:
https://www.researchgate.net/project/One-class-classifiers-based-on-mst

You can find all implementation details in the code. 
Theoretical methods and applied in the arXiv preprint.

## Usage
Install requirements
```
pip install -r requirements.txt
```

Run experiments...
```
# change into src/mst_cd_test.py the variable datasets_name
# datasets_name = '<Dataset Name>'
# Es. datasets_name = 'liver'
python src/mst_cd_test.py
```

## Datasets
In datasets folder you can find alla dataset used in this work. We modified all labels from datasets UCI to adapt in our project not changing the meaning of course.
In covid-19/COVID-19 we have all deep features extracted by a Resnet10 on different k-fold (more details in paper available on arXiv), and in Negative-Covid-19/Positive-Covid-19 you find all x-ray images used for our third experiment.

<img src="images/PRLetters_Dynamic_decision_boundary.jpg" alt="drawing" width="500px"/>

## Results
Performance comparison using the MCC metric on UCI repository.

|  Dataset |OCSVM|OCdmst|
|----------|-----------|-----------|
|Breast (Benign)|0.473|0.774|
|Breast (Malignant)|0.234|0.204|
|Diabetes (Absent)|0.01|0.066|
|Diabetes (Present)|0.203|0.178|
|Glass (Float)|0.389|0.535|
|Glass (NoFloat)|-0.149|0.238|
|Heart (Present)|0.069|0.037|
|Heart (Absent)|-0.005|0.117|
|Liver (Disorder)|0.056|0.099|
|Liver (Healthy)|0.065|0.073|
|Sonar (Mines)|0.133|0.672|
|Sonar (Rocks)|0.054|0.336|
|**Average**|0.128|**0.277**|

## Covid-19 dataset
Performance comparison using the MCC metric on COVID-19 datasets.
In this experiment we train a resnet18 with cross entropy loss and extract the deep features. These last will be used as input to our OCdmst.
Results of OCdmst outpeforms the accuracy of the neural network in scenario with few data.

|  K-Fold |Positive class||Negative class|
|----------|-----------|--|-----------|
|||*OCdmst*||
|1|**0.853**||**0.347**|
|2|**0.843**||**0.187**|
|||*Resnet18*||
|1|0.781||0.|
|2|0.828||0.|

## Citation
More details and results in published work:

```
@misc{grassa2020dynamic,
    title={Dynamic Decision Boundary for One-class Classifiers applied to non-uniformly Sampled Data},
    author={Riccardo La Grassa and Ignazio Gallo and Nicola Landro},
    year={2020},
    eprint={2004.02273},
    archivePrefix={arXiv},
    primaryClass={cs.LG}
}
```


## References
[1] [breast_cancer_wisconsin](http://archive.ics.uci.edu/ml/datasets/Breast+Cancer+Wisconsin+%28Original%29)

[2] [covid-19](hhttps://github.com/ieee8023/covid-chestxray-dataset/commit/51880b5a5427ca63512a906fe743ba3b67fb9711)

[3] [diabetes](http://archive.ics.uci.edu/ml/datasets/Diabetes)

[4] [glass](http://archive.ics.uci.edu/ml/datasets/Glass+Identification)

[5] [heart](http://archive.ics.uci.edu/ml/datasets/Heart+Disease)

[6] [liver](http://archive.ics.uci.edu/ml/datasets/Liver+Disorders)

[7] [sonar](http://archive.ics.uci.edu/ml/datasets/Connectionist+Bench+%28Sonar%2C+Mines+vs.+Rocks%29)

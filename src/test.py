import os

from adjustText import adjust_text
from matplotlib.pyplot import *


def plot_accuracy_epochs(dataset, tmp_mcc,match_gamma, match_depth):
    fig, ax = subplots()
    ax.plot(match_depth, tmp_mcc, label=match_gamma, ls='--', c='black')

    ax.set(xlabel='Depth', ylabel='mcc',
           title='mcc over depth ('+dataset+')')
    xticks(match_depth)
    ax.grid()
    legend(loc='lower right')
    show(bbox_inches='tight', dpi=300)


def plot_error_bar(dataset, tmp_mcc, label):
    fis, ax=subplots()
    texts=[]
    for i in tmp_mcc:
        ax.errorbar(float(i[0]), i[1], fmt='.k', ls='--',zorder=1)
        for idx, (item, depth) in enumerate(zip(i[1],i[2])):
            texts.append(text(float(i[0]), item, depth))
        ax.scatter(float(i[0]), np.min(i[1]), c='green', label="lower limit",zorder=2)
        ax.scatter(float(i[0]), np.mean(i[1]), c='blue',zorder=2)
        ax.scatter(float(i[0]), np.max(i[1]), c='red', label="upper limit",zorder=2)

    ax.plot([float(i[0]) for i in tmp_mcc], [np.mean(i[1]) for i in tmp_mcc], ls='--', c='blue')
    ax.plot([float(i[0]) for i in tmp_mcc], [np.max(i[1]) for i in tmp_mcc], ls='--', c='red')


    ax.set(xlabel='γ', ylabel='mcc',
           title='mcc over depth ('+dataset+')')
    xticks([float(i[0]) for i in tmp_mcc])
    ax.grid()
    #plt.legend(loc='lower right')
    adjust_text(texts)
    savefig('./images/'+dataset+'_'+str(label)+'_k=1')
    #show(dpi=500)




path="/home/nataraja/Desktop/OCdmst_results/label "
regex = ('Total avg mcc:  ')+('\[-?\d+.\d+\]')#+(' dev:  ')+('\d+.\d+')
regex_acc = ('Total avg accuracy:  ')+('\[-?\d+.\d+\]')#+(' dev:  ')+('\d+.\d+')
#regex = ('Total avg mcc:  ')+('-?\d+.\d+')+(' dev:  ')+('\d+.\d+')
#regex_acc = ('Total avg accuracy:  ')+('-?\d+.\d+')+(' dev:  ')+('\d+.\d+')
regex_gamma = ('_Gamma: ')+('\d+')
regex_depth = ('_Depth: ')+('\d+')

def max_performance():
    for label in [1, -1]:
        datasets_list = os.listdir(path +str(label)+'/')
        for i in datasets_list:
            if i!='covid-19':
                tmp_mcc = []
                tmp_dev = []
                dataset_file_list=os.listdir(path+str(label)+'/'+i+'/k=0'+'/')
                for w in dataset_file_list:
                    F=open(path+str(label)+'/'+i+'/'+w)
                    a=F.read()
                    if a:
                        match = re.findall(regex, a)
                        match_acc=re.findall(regex_acc, a)
                        tmp_mcc.append([w, float(match[0].split(' ')[4]), float(match[0].split(' ')[7]), float(match_acc[0].split(' ')[4])])
                    F.close()

                if tmp_mcc:
                    print(max(tmp_mcc, key=lambda x: x[1]))



def delta_var():
    for label in [1]:
        datasets_list = os.listdir(path + str(label) + '/')

        for i in datasets_list:
            if i =='covid-19':
                tmp_mcc = []
                depth_list=[]
                previous_w=0
                #tmp_dev = []
                dataset_file_list = sorted(os.listdir(path + str(label) + '/' + i + '/'))
                for index, w in enumerate(dataset_file_list):
                    F = open(path + str(label) + '/' + i + '/' + w)
                    a = F.read()
                    if a:
                        match_gamma= re.findall(regex_gamma, w)[0].split()[1]
                        match_depth= re.findall(regex_depth, w)[0].split()[1]
                        if match_gamma!=previous_w and index!=0:
                            plot_accuracy_epochs(i, tmp_mcc,match_gamma,depth_list)
                            tmp_mcc=[]
                            depth_list = []

                        match = re.findall(regex, a)
                        previous_w=match_gamma
                        #match_acc = re.findall(regex_acc, a)
                        depth_list.append(int(match_depth))
                        tmp_mcc.append(float(match[0].split(' ')[4]))#,float(match_acc[0].split(' ')[4])])
                    F.close()
                break



def error_bar():
    for label in [1]:
        datasets_list = os.listdir(path + str(label))
        #datasets_list.remove('covid-19')
        for i in datasets_list:
            if i =='covid-19':
                tmp_mcc = []
                depth_list=[]
                bar_list=[]
                previous_w=0
                #tmp_dev = []
                dataset_file_list = os.listdir(path + str(label) + '/'+ i +'/k=1/')
                dataset_file_list=[(q, q.split()[2].split(':')[0],q.split()[3]) for q in dataset_file_list]
                how_many_gamma=set([i[1] for i in dataset_file_list])
                dataset_file_list=sorted(dataset_file_list, key=lambda x: int(x[1]))
                dataset_file_list=[q[0] for q in dataset_file_list]
                for index, w in enumerate(dataset_file_list):
                    F = open(path + str(label) + '/' + i +'/k=1' + '/' + w)
                    a = F.read()
                    if a:
                        match_gamma= re.findall(regex_gamma, w)[0].split()[1]
                        match_depth= re.findall(regex_depth, w)[0].split()[1]


                        if match_gamma!=previous_w and index!=0:
                            bar_list.append([previous_w, tmp_mcc[:], depth_list[:]])

                            #plot_accuracy_epochs(i, tmp_mcc,match_gamma,depth_list)
                            tmp_mcc=[]
                            depth_list = []

                        match = re.findall(regex, a)
                        previous_w=match_gamma
                        #match_acc = re.findall(regex_acc, a)
                        depth_list.append(int(match_depth))
                        tmp_mcc.append(float(match[0].split(' ')[4].replace('[','').replace(']','')))#,float(match_acc[0].split(' ')[4])])
                        if index==len(dataset_file_list)-1:
                            bar_list.append([match_gamma, tmp_mcc[:], depth_list[:]])

                    F.close()
                plot_error_bar(i,bar_list, label)



#if os.path.exists('/home/nataraja/Desktop/research_project/DH-MST_CD/images/'):
#    shutil.rmtree('/home/nataraja/Desktop/research_project/DH-MST_CD/images/')
#    os.mkdir('/home/nataraja/Desktop/research_project/DH-MST_CD/images')
#else:
#    os.mkdir('/home/nataraja/Desktop/research_project/DH-MST_CD/images')
#max_performance()
error_bar()


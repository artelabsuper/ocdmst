# -*- coding: utf-8 -*-
from __future__ import division  # to force division to floating point

import itertools
import math
import re
import sys
import time

from matplotlib import pyplot as plt
from mlxtend.plotting import plot_confusion_matrix

epsilon = sys.float_info.epsilon
import networkx as nx
import numpy as np
from scipy.spatial import distance
from sklearn.metrics import accuracy_score
from src import utils

VERBOSE = False


def save_file(path, results):
    try:
        outF = open(path, "a+")
        for i in results:
            outF.writelines(str(i))
            outF.write('\n')
        outF.close()
        print("File saved.")
    except Exception as e:
        print("Error save results function", e)


def centeroidnp(arr, dim_feat):
    length = len(arr)
    return np.array([(np.sum(arr[:, i]) / length) for i in range(dim_feat)])


def create_small_mst(g0_weight_sorted, gamma_index, acc):
    complete_g0 = nx.Graph()
    complete_g0.add_nodes_from([i[0] for i in g0_weight_sorted[:gamma_index]])
    edges_couples = [(u, v) for u, v in itertools.product(nx.nodes(complete_g0), nx.nodes(complete_g0)) if u != v]
    for u, v in edges_couples:
        complete_g0.add_edge(u, v, weight=distance.euclidean(acc[u], acc[v]))

    small_mst0 = nx.minimum_spanning_tree(complete_g0)
    return small_mst0


def dynamic_theta(small_g0, idx_near_node, depth, acc):
    small_g1 = []
    std_random_crown_list = []
    bfs_graph = nx.bfs_tree(small_g0, idx_near_node, depth_limit=depth)

    for j in range(0, 100):
        random_nodes = np.random.choice(small_g0.nodes, len(bfs_graph), replace=False)
        dat_random = np.asarray([acc[node] for node in random_nodes])
        std_random_crown, var_rand = utils.std_2d(dat_random)
        std_random_crown_list.append(std_random_crown)

    dat = np.asarray([acc[node] for node in bfs_graph.nodes])

    for u, v in bfs_graph.edges:
        small_g1.append(distance.euclidean(acc[u], acc[v]))

    median_bfs_tree_edges = np.median(small_g1)
    # mad = utils.mad_2d(dat)

    std_crown, dat_mean = utils.std_2d(dat)
    # return centeroidnp(dat,len(acc[0])),std_crown
    return median_bfs_tree_edges, std_crown, np.median(std_random_crown_list), np.max(std_random_crown_list)


def main_mst_cd_gp(acc, X_test, y_test, k_fold_acc, auc_list, mcc_list, gamma, truth_class, outliers_label, depth,
                   x_max, x_min):
    '''local_path = "/home/super/PycharmProjects/rlagrassa/datasets_uci/"+datasets_name+'/'
    #local_path = "/home/nataraja/Desktop/datasets/" + datasets_name + '/'
    raw = open(local_path + datasets_name.upper() + '/' + datasets_name + '_train.data', 'rt')
    train = np.loadtxt(raw)
    #tr_size_list=[int(len(train)/10), int(len(train)/8), int(len(train)/6), int(len(train)/4), int(len(train)/2), int(len(train))]
    tr_size_list=[int(len(train))]'''

    # print("Size positive train: ", len(acc), "Size negative train: ", len(unacc), "Label negative: ", len(acc_label),"Label positive: ", len(unacc_label), "Test size: ", len(X_test), "Label size: ", len(y_test))

    g0 = nx.Graph()

    for i in range(0, len(acc)):
        g0.add_node(i)

    # prediction test
    TN = 0
    FP = 0
    TP = 0
    FN = 0

    y_pred = []
    for j in range(0, len(X_test)):
        new_vectorize = X_test[j]
        all_distance_positive_train = []

        start_time = time.time()
        for node0 in nx.nodes(g0):
            all_distance_positive_train.append(
                [node0, distance.euclidean(new_vectorize, acc[node0])])

        g0_weight_sorted = sorted(all_distance_positive_train, key=lambda x: x[1])

        small_mst0 = create_small_mst(g0_weight_sorted, gamma, acc)

        # search projection
        dist0 = {}
        dist0_euclidean = {}
        for c0 in small_mst0.edges(data=True):
            x_j = acc[c0[1]]
            x_i = acc[c0[0]]
            x = new_vectorize
            # if c0[2]['weight'] <= theta:
            denominator = distance.sqeuclidean(x_j, x_i) + epsilon
            degree = (np.dot(np.transpose(np.subtract(x_j, x_i)), np.subtract(x, x_i))) / denominator
            degree = (np.abs(degree))

            if 0. <= degree <= 1.:
                projection_point = np.add(x_i, (
                    np.dot(((np.dot(np.transpose(np.subtract(x_j, x_i)), np.subtract(x, x_i))) / denominator),
                           (np.subtract(x_j, x_i)))))
                dist0[(c0[0], c0[1])] = distance.euclidean(x, projection_point)
            else:
                a = distance.euclidean(x_i, x)
                b = distance.euclidean(x_j, x)
                if a <= b:
                    best_node = c0[0]
                    min_best_node = a
                else:
                    best_node = c0[1]
                    min_best_node = b
                dist0_euclidean[best_node] = min_best_node

        if dist0_euclidean and dist0:
            near_edge = (min(dist0, key=dist0.get))
            near_node = (min(dist0_euclidean, key=dist0_euclidean.get))

            if dist0[near_edge] < dist0_euclidean[near_node]:
                min_dist0 = dist0[near_edge]
                idx_near_node = near_edge[0]
                centroid_bfs, std_crown, std_random_bfs, max_std_value = dynamic_theta(small_mst0, idx_near_node, depth,
                                                                                       acc)
            else:
                min_dist0 = dist0_euclidean[near_node]
                idx_near_node = near_node
                centroid_bfs, std_crown, std_random_bfs, max_std_value = dynamic_theta(small_mst0, idx_near_node, depth,
                                                                                       acc)
        else:
            if not dist0_euclidean and dist0:
                near_edge = (min(dist0, key=dist0.get))
                min_dist0 = dist0[near_edge]
                idx_near_node = near_edge[0]  # !TODO Attention please! do not forget to change node
                centroid_bfs, std_crown, std_random_bfs, max_std_value = dynamic_theta(small_mst0, idx_near_node, depth,
                                                                                       acc)
            elif not dist0 and dist0_euclidean:
                near_node = (min(dist0_euclidean, key=dist0_euclidean.get))
                min_dist0 = dist0_euclidean[near_node]
                idx_near_node = near_node
                centroid_bfs, std_crown, std_random_bfs, max_std_value = dynamic_theta(small_mst0, idx_near_node, depth,
                                                                                       acc)

        std_normalize = (std_crown / (np.sqrt(((x_max - x_min) ** 2) / 4)))
        std_rand_norm = (std_random_bfs / (np.sqrt(((x_max - x_min) ** 2) / 4)))
        max_std_value_norm = (max_std_value / (np.sqrt(((x_max - x_min) ** 2) / 4)))

        # print(std_normalize)
        # if mad==0:
        #     theta = centroid_bfs
        #
        # else:
        #     mad_norm=mad/std_crown
        #     decimal = len(re.search('\d+\.(0*)', str(mad_norm)).group(1))
        #     normalizer = 5 * (10 ** -decimal)
        #     theta = centroid_bfs * ((np.e ** -(mad_norm / normalizer)))
        # print(std_normalize, 1-std_normalize)
        # if mad_normalize>1.:
        #    print("Error")
        #    exit()
        # print(std_normalize)
        # if std_normalize>1.0:
        #     std_normalize=1.
        # if std_normalize < 0.3:
        # #     print("Pumping")
        #      theta = centroid_bfs *1.2
        # else:
        #     theta = centroid_bfs  * ((1-std_normalize))
        #     #print("De-Pumping")
        # decimal=len(re.search('\d+\.(0*)', str(std_normalize)).group(1))
        # decimal = len(re.search('\d+\.(0*)', str(std_normalize)).group(1))
        # normalizer=5*(10**-decimal)

        # print(std_normalize)
        # K_normalizer = (10 ** 3)
        # decimal = len(re.search('\d+\.(0*)', str(max_std_value_norm)).group(1)) + 2
        # K_normalizer = (20 ** decimal)

        decimal = len(re.search('\d+\.(0*)', str(std_rand_norm)).group(1))
        # if decimal==0:
        #     k=30
        #     grown_rate = 1.05
        # elif decimal==1:
        #     k=20
        #     grown_rate = 1.25
        # elif decimal==2:
        #     k=10
        #     grown_rate = 1.5
        # elif decimal==3:
        #     print("Errore about zeros")
        #     exit()
        k = 6
        grown_rate = 1.9
        K_normalizer = k / (std_rand_norm)

        # if std_normalize < std_rand_norm:
        #    theta = centroid_bfs
        # else:
        theta = centroid_bfs * (1 / (1 + np.e ** (K_normalizer * (std_normalize - std_rand_norm * grown_rate))))
        # theta = centroid_bfs  * ((np.e**-(std_normalize/.07)))
        # print(min_dist0, centroid_bfs, theta,std_normalize, std_rand_norm)

        if min_dist0 <= theta:
            y_pred.append(truth_class)
            if y_test[j] == truth_class:
                TP = TP + 1
            else:
                FP = FP + 1

        elif min_dist0 > theta:
            y_pred.append(outliers_label)
            if y_test[j] == truth_class:
                FN = FN + 1
            else:
                TN = TN + 1

        # elif not dist0:
        #     min_dist0_euclidean = min(dist0_euclidean)
        #     if min_dist0_euclidean > theta:
        #         y_pred.append(-1)
        #         if y_test[j] == 1:
        #             FN = FN + 1
        #         elif y_test[j] == -1:
        #             TN = TN + 1
        #
        #
        #
        #     elif min_dist0_euclidean <= theta:
        #         y_pred.append(1)
        #         if y_test[j] == 1:
        #             TP = TP + 1
        #         elif y_test[j] == -1:
        #             FP = FP + 1

    try:
        end_time = time.time()
        confusion_matrix = np.zeros((2, 2), dtype=int)
        print(TP, TN, FP, FN)
        if ((TP * TN) - (FP * FN)) != 0:
            mcc = ((TP * TN) - (FP * FN)) / (math.sqrt((TP + FP) * (TP + FN) * (TN + FP) * (TN + FN)))
        else:
            mcc = 0.

        try:
            acc0 = (TP + TN) / (TP + TN + FN + FP)
        except Exception as e:
            acc0 = 0.

        for t, p in zip(y_pred, y_test):
            if t == -1:
                t = 0
            if p == -1:
                p = 0
            confusion_matrix[int(t), int(p)] += 1

        print(confusion_matrix)
        print(100 * confusion_matrix.diagonal() / confusion_matrix.sum(1))
        # # print(100*confusion_matrix.diagonal().sum()/confusion_matrix.sum())

        fig, ax = plot_confusion_matrix(conf_mat=confusion_matrix, colorbar=True, figsize=(3, 3.1))
        fig.savefig("./images/covid-19/" + "/OCdmst_confusion_matrix_" + str(0) + str(
            gamma) + "_" + str(depth) + " " + str(truth_class) + "_" + ".png")
        plt.close()

        # Sensitivity0 = TP / (TP + FN)
        # Precision0 = TP / (TP + FP)
        # f0_score = (2 * Precision0 * Sensitivity0)/(Precision0+Sensitivity0)
        # conf_matrix=np.array([[TP, FN],[FP, TN]])
        # auc=(roc_auc_score(y_test, y_pred))
        # auc_list.append(100*auc)
        # if VERBOSE:
        #     print("Time: ",end_time-start_time)
        #     print("AUC: ",auc)
        #     print("Sensitivity:",Sensitivity0, "Precision: ",Precision0, "f1score: ",f0_score,"Accuracy: ", acc0)
        #     print("Confusion matrix:")
        #     print(conf_matrix)
        #     # results=classification_report(y_test, y_pred, target_names=['-1', '1'])
        #     print("")
        k_fold_acc.append(100 * acc0)
        mcc_list.append(mcc)
    except Exception as e:
        print(e)
        exit()
        # auc_list.append( 100* roc_auc_score(y_test, y_pred))
        k_fold_acc.append(100 * accuracy_score(y_test, y_pred))
        mcc_list.append(mcc)

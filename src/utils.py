import numpy as np
import os
import sys
import errno
import os.path as osp

def mkdir_if_missing(directory):
    if not osp.exists(directory):
        try:
            os.makedirs(directory)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise



class Logger(object):
    def __init__(self, fpath=None):
        self.file = None
        self.terminal = sys.stdout
        if fpath is not None:
            mkdir_if_missing(os.path.dirname(fpath))
            self.file = open(fpath, 'w')

    def write(self, msg):
        self.terminal.write(msg)
        if self.file is not None:
            self.file.write(msg)

    def flush(self):
        if self.file is not None:
            self.file.flush()
            os.fsync(self.file.fileno())

    def close(self):
        if self.file is not None:
            self.file.close()




def centeroidnp(arr, dim_feat):
    length = len(arr)
    return  np.sum(arr, axis=0)/length



def std_2d(a):
    dimension=a.shape[1]
    x_mean=centeroidnp(a, dimension)
    var=np.sum(np.subtract(a,x_mean)**2)/(a.shape[1]*a.shape[0])
    #var=np.sum(np.sum(np.subtract(a,x_mean)**2, axis=0))/a.shape[0]
    return np.sqrt(var),var

def mad_2d(a):
    dimension=a.shape[1]
    x_mean=centeroidnp(a, dimension)
    mad=np.sum(np.abs(np.subtract(a,x_mean)))/(a.shape[0]*a.shape[1])
    return mad


# #
# a=np.array([[0.0, 0.0, 0.0], [2, 2, 2]])
# std, var=(std_2d(a))
# std_norm=std/(np.sqrt(3*.25))
#
#

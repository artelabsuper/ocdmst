import itertools
import re
import sys

from matplotlib.pyplot import subplots, legend

epsilon = sys.float_info.epsilon

import networkx as nx
import numpy as np
from scipy.spatial import distance

from src import utils


def centeroidnp(arr, dim_feat):
    length = len(arr)
    return np.sum(arr, axis=0) / length


def std_2d(a):
    dimension = a.shape[1]
    x_mean = centeroidnp(a, dimension)
    var = np.sum(np.subtract(a, x_mean) ** 2) / (a.shape[1] * a.shape[0])
    return np.sqrt(var)


def mad_2d(a):
    dimension = a.shape[1]
    x_mean = centeroidnp(a, dimension)
    mad = np.sum(np.abs(np.subtract(a, x_mean))) / (a.shape[0] * a.shape[1])
    return mad


def create_small_mst(g0_weight_sorted, gamma_index, acc):
    complete_g0 = nx.Graph()
    complete_g0.add_nodes_from([i[0] for i in g0_weight_sorted[:gamma_index]])
    edges_couples = [(u, v) for u, v in itertools.product(nx.nodes(complete_g0), nx.nodes(complete_g0)) if u != v]
    for u, v in edges_couples:
        complete_g0.add_edge(u, v, weight=distance.euclidean(acc[u], acc[v]))

    small_mst0 = nx.minimum_spanning_tree(complete_g0)
    return small_mst0


def dynamic_theta(small_g0, idx_near_node, depth, acc):
    small_g1 = []
    bfs_graph = nx.bfs_tree(small_g0, idx_near_node, depth_limit=depth)
    # print("Oriignal size: ",len(small_g0),"Now: ",len(bfs_graph))
    dat = np.asarray([acc[node] for node in bfs_graph.nodes])

    for u, v in bfs_graph.edges:
        small_g1.append(distance.euclidean(acc[u], acc[v]))

    median_bfs_tree_edges = np.median(small_g1)
    mad = utils.mad_2d(dat)

    std_crown, dat_mean = utils.std_2d(dat)
    # return centeroidnp(dat,len(acc[0])),std_crown
    return median_bfs_tree_edges, std_crown, mad


def main_mst_cd_gp():
    acc = np.random.rand(100, 3)
    X_test = np.random.rand(1, 3)
    gamma = 4
    depth = 1
    g0 = nx.Graph()

    for i in range(0, len(acc)):
        g0.add_node(i)

    # prediction test
    TN = 0
    FP = 0
    TP = 0
    FN = 0

    y_pred = []
    for j in range(0, len(X_test)):
        new_vectorize = X_test[j]
        all_distance_positive_train = []

        for node0 in nx.nodes(g0):
            all_distance_positive_train.append(
                [node0, distance.euclidean(new_vectorize, acc[node0])])

        g0_weight_sorted = sorted(all_distance_positive_train, key=lambda x: x[1])
        small_mst0 = create_small_mst(g0_weight_sorted, gamma, acc)

        # search projection
        dist0 = {}
        dist0_euclidean = {}
        for c0 in small_mst0.edges(data=True):
            x_j = acc[c0[1]]
            x_i = acc[c0[0]]
            x = new_vectorize
            denominator = distance.sqeuclidean(x_j, x_i) + epsilon
            degree = (np.dot(np.transpose(np.subtract(x_j, x_i)), np.subtract(x, x_i))) / denominator
            degree = (np.abs(degree))

            if 0. <= degree <= 1.:
                projection_point = np.add(x_i, (
                    np.dot(((np.dot(np.transpose(np.subtract(x_j, x_i)), np.subtract(x, x_i))) / denominator),
                           (np.subtract(x_j, x_i)))))
                dist0[(c0[0], c0[1])] = distance.euclidean(x, projection_point)
            else:
                a = distance.euclidean(x_i, x)
                b = distance.euclidean(x_j, x)
                if a <= b:
                    best_node = c0[0]
                    min_best_node = a
                else:
                    best_node = c0[1]
                    min_best_node = b
                dist0_euclidean[best_node] = min_best_node
            # else:
            # dist0_euclidean.append(min(distance.euclidean(x_j, x), distance.euclidean(x_i, x)))

        if dist0_euclidean and dist0:
            near_edge = (min(dist0, key=dist0.get))
            near_node = (min(dist0_euclidean, key=dist0_euclidean.get))

            if dist0[near_edge] < dist0_euclidean[near_node]:
                min_dist0 = dist0[near_edge]
                idx_near_node = near_edge[0]
                centroid_bfs, std_crown, mad = dynamic_theta(small_mst0, idx_near_node, depth, acc)
            else:
                min_dist0 = dist0_euclidean[near_node]
                idx_near_node = near_node
                centroid_bfs, std_crown, mad = dynamic_theta(small_mst0, idx_near_node, depth, acc)
        else:
            if not dist0_euclidean and dist0:
                near_edge = (min(dist0, key=dist0.get))
                min_dist0 = dist0[near_edge]
                idx_near_node = near_edge[0]  # !TODO Attention please! do not forget to change node
                centroid_bfs, std_crown, mad = dynamic_theta(small_mst0, idx_near_node, depth, acc)
            elif not dist0 and dist0_euclidean:
                near_node = (min(dist0_euclidean, key=dist0_euclidean.get))
                min_dist0 = dist0_euclidean[near_node]
                idx_near_node = near_node
                centroid_bfs, std_crown, mad = dynamic_theta(small_mst0, idx_near_node, depth, acc)

        std_normalize = (std_crown / (np.sqrt(((1 - 0) ** 2) / 4)))
        # if mad==0:
        #     theta = centroid_bfs
        #
        # else:
        #     mad_norm=mad/std_crown
        #     decimal = len(re.search('\d+\.(0*)', str(mad_norm)).group(1))
        #     normalizer = 5 * (10 ** -decimal)
        #     theta = centroid_bfs * ((np.e ** -(mad_norm / normalizer)))
        # print(std_normalize, 1-std_normalize)
        # if mad_normalize>1.:
        #    print("Error")
        #    exit()
        # print(std_normalize)
        # if std_normalize>1.0:
        #     std_normalize=1.
        # if std_normalize < 0.3:
        # #     print("Pumping")
        #      theta = centroid_bfs *1.2
        # else:
        #     theta = centroid_bfs  * ((1-std_normalize))
        #     #print("De-Pumping")
        # decimal=len(re.search('\d+\.(0*)', str(std_normalize)).group(1))
        decimal = len(re.search('\d+\.(0*)', str(std_normalize)).group(1))
        normalizer = 5 * (10 ** -decimal)
        theta = centroid_bfs * ((np.e ** -(std_normalize / normalizer)))
        print(min_dist0, theta, std_normalize, centroid_bfs)


def plot_accuracy_epochs(domain, theta_list, std_median):
    fig, ax = subplots()
    for i in theta_list:
        print(i)
        ax.plot(domain, i[1], ls='-', label='k=' + str(i[2]), fillstyle='none')

    ax.set(xlabel='Domain', ylabel='Range',
           title='Inverted Logistic function')
    # xticks(domain)
    ax.axvline(x=std_median, ls='--', c='black', linewidth=2.0)
    ax.grid()
    legend(loc='upper right')
    fig.savefig('./images/shift_curve.png')
    # show(bbox_inches='tight', dpi=400)


theta_list = []
domain = np.arange(.01, .05, 0.00001)
for k in [6, 7, 8, 9, 10, 20]:
    tmp = []
    for i in domain:
        # std_rand=.001
        std_median = .025
        std_normalize = (i / (np.sqrt(((1 - 0) ** 2) / 4)))
        median_std_value_norm = (std_median / (np.sqrt(((1 - 0) ** 2) / 4)))
        # if i < std_rand:
        #     theta = 1
        # else:
        # decimal = len(re.search('\d+\.(0*)', str(median_std_value_norm)).group(1))+2
        K_normalizer = k / (median_std_value_norm)
        grown_rate = 1.5
        # print(i, normaliz4r)
        # theta = 1/(1+ np.e**-(40*(i-0.4)))
        tmp.append(1 / (1 + np.e ** (K_normalizer * (std_normalize - median_std_value_norm * grown_rate))))
        # theta =((np.e ** -(i / .01)))
    theta_list.append([grown_rate, tmp[:], k])

plot_accuracy_epochs(domain / (np.sqrt(((1 - 0) ** 2) / 4)), theta_list, median_std_value_norm)

import re
import os
from matplotlib import pyplot as plt


def plot_accuracy_epochs():
    fig, ax = plt.subplots()
    for index, i in enumerate(acc_list):
        ax.plot([int(i) for i in range(len(i))], i, label=list_dir[index])

    ax.set(xlabel='Epochs', ylabel='Accuracy',
           title='mcc over epochs')
    ax.grid()
    plt.legend(loc='lower right')
    plt.show(bbox_inches='tight', dpi=300)
    # dirname = osp.join(args.save_dir)
    # fig.savefig(dirname + "/accuracy_over_epochs_deep_" + args.loss + "_" + str(args.deep_level) + "_" + str(args.batch_size) + " lr:" + str(args.lr) + " lr_cent" + str(args.lr_cent) + ".png")


path = "./diff/"
# path="/home/nataraja/Desktop/transfer_paper/"
list_dir = os.listdir(path)
# list_dir.remove('README')
# list_dir.remove('lr_0,01')
# list_dir.remove('old_withoutLoss')
# list_dir.remove('others_experiments (last)')

acc_list = []
regex = ('Total avg mcc:  ') + ('-?\d+.\d+')

for i in list_dir:
    F = (open(path + i, 'r'))
    match = re.findall(regex, F.read())
    tmp_acc = [float((i.split(': ')[1])) for i in match]
    acc_list.append(tmp_acc[:])
    F.close()

plot_accuracy_epochs()

# -*- coding: utf-8 -*-
from __future__ import division  # to force division to floating point

import os.path as osp
import random
from collections import Counter

import numpy as np
from src import utils
from sklearn.model_selection import KFold

from src import dh_mst_cd

VERBOSE=False

datasets_name = 'covid-19'
hold_out=20
local_path = "./datasets/" + datasets_name + '/'
local_project="./logs/"

# Load dataset
try:
    raw = open(local_path + datasets_name.upper() + '/' + datasets_name + '_train_1.data', 'rt')
    train = np.loadtxt(raw)
    raw = open(local_path + datasets_name.upper() + '/' + datasets_name + '_train_1.labels', 'rt')
    train_label = np.loadtxt(raw)
    print("Dataset size: ", len(train), "", Counter(train_label))

    if datasets_name =='covid-19':
        raw = open(local_path + datasets_name.upper() + '/' + datasets_name + '_test_1.data', 'rt')
        raw_test = np.loadtxt(raw)
        raw = open(local_path + datasets_name.upper() + '/' + datasets_name + '_test_1.labels', 'rt')
        raw_test_label = np.loadtxt(raw)
        print("Dataset size: ", len(raw_test), "", Counter(raw_test_label))

except Exception as e:
    print(e)
    print("Loading datasetes failed")
    exit()

for i in [1]:
    truth_class, outliers_label = 1.* i, -1. * i
    n_instances_truth_class=Counter(train_label)[truth_class]

    #for w1 in range(0,1):
    for gamma in np.arange(int(np.round(n_instances_truth_class/4)),int(np.round(n_instances_truth_class/2)),5): #int(np.round(n_instances_truth_class/4)),int(np.round(n_instances_truth_class/2))
        for depth in np.arange(1, 11, 1): #int(np.ceil(gamma/5))
        #for w in range(0,1)
            #gamma=18
            #depth=1
            results_list = []
            file_output = utils.Logger(osp.join(local_project, 'Dataset: ' + datasets_name + "_Gamma: " +str(gamma) + ":_Depth: " +str(depth) + " Truth: " + str(truth_class)))
            print("Dataset: {:} Number of Experiments: {:} Gamma: {:} BFS depth: {:} target class: {:}".format(datasets_name,hold_out,gamma, depth,truth_class),file=file_output)

            avg_total_accuracy=[]
            avg_total_auc=[]
            avg_total_mcc=[]

            train_target_class,outliers_class= [], []
            #min_max_scaler = preprocessing.MinMaxScaler()
            #train = min_max_scaler.fit_transform(train)
            x_max, x_min=np.max(train),np.min(train)

            c = list(zip(train, train_label))
            random.seed(np.random.randint(0, 100000))
            random.shuffle(c)
            train_data, tr_labels = zip(*c)
            train_data, tr_labels=list(train_data), list(tr_labels)

            # for object, label in zip(train_data, tr_labels):
            #     if label == truth_class:
            #         acc.append(object)
            #     else:
            #         unacc.append(object)

            #train_data_samples=acc[0:int(np.around(len(acc)*70/100))]
            #y_train=np.full(int(np.around(len(acc)*70/100)), truth_class)

            #test_data_samples = np.concatenate((acc[int(np.around(len(acc)*70/100)):], unacc))
            #y_test=np.concatenate((np.full(len(acc) - int(np.around(len(acc)*70/100)), truth_class), np.full(len(unacc), outliers_label)))


            for data, label in zip(train_data, tr_labels):
                if label == truth_class:
                    train_target_class.append(data)
                else:
                    outliers_class.append(data)

            train_target_class=np.array(train_target_class)
            outliers_class=np.array(outliers_class)


            if datasets_name!='covid-19':
                for n_hold_out in(range(0,hold_out)):
                    kf = KFold(n_splits=5, shuffle=True)
                    acc_list, mcc_list= [], []
                    auc_list=[]


                    for train_index, test_index in kf.split(train_target_class):
                        acc = []
                        acc_label = []
                        train_data_samples, X_test = train_target_class[train_index], train_target_class[test_index]
                        test_data_samples = np.concatenate((X_test, outliers_class))
                        y_test=np.concatenate((np.full(len(X_test), truth_class), np.full(len(outliers_class), outliers_label)))

                        dh_mst_cd.main_mst_cd_gp(train_data_samples, test_data_samples, y_test,acc_list,auc_list, mcc_list, gamma, truth_class, outliers_label, depth, x_max, x_min)

                    if VERBOSE:
                        print("All accuracies per k-fold: ",acc_list)
                        print("Mean: ", np.mean(acc_list))
                        print("All auc scores per k-fold: ",auc_list)
                        print("Mean Auc: ", np.mean(auc_list))

                    avg_total_accuracy.append(np.mean(acc_list))
                    avg_total_auc.append(np.mean(auc_list))
                    avg_total_mcc.append(np.mean(mcc_list))

                print("Dataset: ",datasets_name)
                print("Total avg accuracy: ",np.mean(avg_total_accuracy),"dev: ",np.var(avg_total_accuracy),file=file_output)
                print(avg_total_accuracy)
                print("Total avg auc: ",np.mean(avg_total_auc),"dev: ",np.var(avg_total_auc),file=file_output)
                print(avg_total_auc)
                print("Total avg mcc: ",np.mean(avg_total_mcc),"dev: ",np.var(avg_total_mcc),file=file_output)
                print(avg_total_mcc)
                print("*************************************",file=file_output)

            else:
                acc_list, mcc_list,auc_list = [], [], []
                raw_test = np.array(raw_test)
                raw_test_label = np.array(raw_test_label)
                dh_mst_cd.main_mst_cd_gp(train_target_class, raw_test, raw_test_label, acc_list, auc_list, mcc_list, gamma,truth_class, outliers_label, depth, x_max, x_min)
                print("Total avg accuracy: ",acc_list,file=file_output)
                print("Total avg mcc: ",mcc_list,file=file_output)
